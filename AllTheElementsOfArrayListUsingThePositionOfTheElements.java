package Collection;
import java.util.ArrayList;

//Write a Java program to print all the elements of a ArrayList using the position of the elements.

public class AllTheElementsOfArrayListUsingThePositionOfTheElements {
  public static void main(String[] args) {
ArrayList <String> c1 = new ArrayList <String> ();
c1.add("Red");
c1.add("Green");
c1.add("Black");
c1.add("White");
c1.add("Pink");
System.out.println("array list: " + c1);
System.out.println("index of an element: ");
int n = c1.size();
for (int index = 0; index <  n; index++)
 System.out.println(c1.get(index));
}
}
