package Collection;
//Write a Java program to increase the size of an array list.

	import java.util.ArrayList;

	  public class IncreaseTheSizeOfAnArrayList {
	  public static void main(String[] args) {
	          ArrayList<String> c1= new ArrayList<String>();
	          c1.add("Red");
	          c1.add("Green");
	          c1.add("Black");
	          System.out.println("Original array list: " + c1);
	          c1.ensureCapacity(6);
	          c1.add("White");
	          c1.add("Pink");
	          c1.add("yellow");
	          
	  System.out.println("New arraylist "+c1);
	  }   
}
